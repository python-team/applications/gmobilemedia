#!/usr/bin/env python

import os, sys


CURRENT_PATH = sys.path[0]

GLADE_FILE = CURRENT_PATH + os.sep + "ui/interface.glade"

TEXT_DOMAIN = "gmobilemedia"

LOCALE_DIR  = "./locale"

# Association between file extensions and icons
MIME_EXTENSIONS = {
		"txt": {"image": "file-text", "type": "text"},
		"jpg": {"image": "file-image", "type": "image"},
		"jpeg": {"image": "file-image", "type": "image"},
		"gif": {"image": "file-image", "type": "image"},
		"bmp": {"image": "file-image", "type": "image"},
		"mid": {"image": "file-sound", "type": "audio"},
		"midi": {"image": "file-sound", "type": "audio"},
		"swf": {"image": "file-swf", "type": "flash"},
		"jad": {"image": "file-unknown", "type": "text"},
		"jar" : {"image": "file-package", "type": "jar"},
		"mp3" : {"image": "file-sound", "type": "audio"},
		"amr": {"image": "file-sound", "type": "audio"},
		"rms": {"image": "file-unknown", "type": "binary"},
		"avi": {"image": "file-video", "type": "video"},
		"3gp": {"image": "file-video", "type": "video"},
		"mpg": {"image": "file-video", "type": "video"},
		"mp4": {"image": "file-video", "type": "video"},
		"unknown": {"image": "file-unknown", "type": "binary"},
		}
AVAILABLE_CONNECTIONS = ('at','at19200','at115200','atblue','dku2','dku5','dlr3','dlr3blue','fbus','irda','infrared','mbus','obex')

if sys.platform == 'win32':
	SERIAL_PORTS = ('COM1:','COM2:','COM3:','COM4:','COM5:','COM6:','COM7:','COM8:','COM9:','COM10:',)
else:
	SERIAL_PORTS = ('/dev/ttyS0','/dev/ttyS1','/dev/ttyUSB0','/dev/ttyUSB1','/dev/ttyACM0','/dev/ttyACM1','/dev/usb/tts/0','/dev/usb/tts/1',)

