
#!/usr/bin/env python
# -*- coding: utf-8 -*-

###
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###


import sys, os, user, locale
import logging
from helper import *
from urlparse import *
import configuration
from preferences import *
import gettext

locale.setlocale(locale.LC_ALL, '')
gettext.install(configuration.TEXT_DOMAIN, configuration.LOCALE_DIR, unicode=1)
gtk.glade.bindtextdomain(configuration.TEXT_DOMAIN, configuration.LOCALE_DIR)

try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

try:
	from engine import *
except ImportError:
	message_box(_("Error"), _("You have not installed python-gammu it is required for this application to work"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)


def load_icon(icon_name):
	"Loads an image from the icons folder"
	
	icon_name = "ui/icons/"+ icon_name +".svg"
	return gtk.gdk.pixbuf_new_from_file_at_size(icon_name, 32, 32)

class mbGui:
	windowName = ""
	CurrentFolder = u''
	language = 'en'
	engine = None
	settings = None
	

	def __init__( self ):
		
		logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
		
		self.language = gtk.get_default_language().to_string()
		
		# Main objects that control communication and configuration
		self.engine = MobileBrowser()
		self.settings = AppSettings()
		
		self.windowname = "wndPrincipal"
		self.wTree = gtk.glade.XML(configuration.GLADE_FILE, self.windowname, configuration.TEXT_DOMAIN)
		self.win = self.wTree.get_widget(self.windowname)

		#Image control to show the preview
		self.ImageControl = self.wTree.get_widget("previewImage")
		
		#Status label on top
		self.lblStatus = self.wTree.get_widget("lblStatus")

		# Statusbar for general messages
		self.statusbar = self.wTree.get_widget("mainStatus")
		
		#Treestore to keep folders organization
		self.treestore = gtk.TreeStore(str, str, str)

		self.vTree = self.wTree.get_widget("trItems")
		self.vTree.set_model(self.treestore)

		self.folder_icon = gtk.CellRendererPixbuf()
		self.folder_icon.set_property("height",33)
		self.folder_icon.set_property("width",33)

		self.folder_icon.set_property("pixbuf", load_icon("folder-normal"))
		
		for item in configuration.MIME_EXTENSIONS.keys():
			configuration.MIME_EXTENSIONS[item]['pixbuf'] = load_icon(configuration.MIME_EXTENSIONS[item]['image'])

		self.column_folder = gtk.TreeViewColumn()
		self.column_folder.pack_start(self.folder_icon,True)
		self.vTree.append_column(self.column_folder)

		self.tvcolumn = gtk.TreeViewColumn('Carpetas')

		self.vTree.append_column(self.tvcolumn)

		# create a CellRendererText to render the data
		self.cell = gtk.CellRendererText()

		# add the cell to the tvcolumn and allow it to expand
		self.tvcolumn.pack_start(self.cell, True)

		# set the cell "text" attribute to column 0 - retrieve text
		# from that column in treestore
		self.tvcolumn.add_attribute(self.cell, 'text', 0)

		# make it searchable
		self.vTree.set_search_column(0)

		self.vTree.get_selection().connect("changed", self.FolderSelect)

		#ListStore for iconview
		self.filestore = gtk.ListStore(str, gtk.gdk.Pixbuf, str)

		self.fTree = self.wTree.get_widget("iconItems")
		self.fTree.set_model(self.filestore)
		self.fTree.set_text_column(0)
		self.fTree.set_pixbuf_column(1)
		self.fTree.set_item_width(160)
		self.fTree.set_columns(-1)

		self.win.show()
		eventos = {"gtk_main_quit" : gtk.main_quit,
			"on_salir1_activate" : gtk.main_quit,
			"on_mnuSalir_activate" : gtk.main_quit,
			"on_acerca_de1_activate": self.help_about,
			"on_btnConnect_clicked" : self.Conectar,
			"on_iconItems_selection_changed": self.FileInfo,
			"on_iconItems_key_press_event": self.icon_key_press,
			"on_btnRefresh_clicked": self.refresh,
			"on_btPrefs_clicked":	self.preferences,
			}

		#on_iconItems_item_activated"
		self.wTree.signal_autoconnect(eventos)

		#Just testing
		self.TARGET_URL = 80;
		self.TARGETS = [('text/uri-list', 0, self.TARGET_URL),]
		
		#let user drag items from the widget
		self.fTree.enable_model_drag_source(gtk.gdk.BUTTON1_MASK, self.TARGETS, gtk.gdk.ACTION_DEFAULT|gtk.gdk.ACTION_MOVE)
		self.fTree.drag_source_set(gtk.gdk.BUTTON1_MASK, self.TARGETS, gtk.gdk.ACTION_DEFAULT|gtk.gdk.ACTION_COPY)

		self.fTree.connect("drag_data_get", self.on_iconItems_drag_data_get)
		
		#Makes the widget able to receive dragged items
		self.fTree.enable_model_drag_dest(self.TARGETS, gtk.gdk.ACTION_DEFAULT)
		self.fTree.drag_dest_set(gtk.DEST_DEFAULT_MOTION |  gtk.DEST_DEFAULT_HIGHLIGHT,
							self.TARGETS,
							gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_MOVE)
		self.fTree.connect('drag_data_received', self.drag_data_received)

	def on_iconItems_drag_data_get( self, widget, context, selection, targetType, eventTime):
		
		#~ if (context.source_window.get_parent() == context.dest_window.get_parent()):
			#~ context.drag_abort(eventTime)
			#~ return 
		
		if targetType== self.TARGET_URL:
			current_selection = widget.get_selected_items()
			total_items = len(current_selection)
			logging.debug("Number of items selected: %s" % total_items)
			
			if total_items>0:
				file_list = []
				
				my_fraction = 1 / total_items
				
				progreso = HigProgress()

				progreso.primary_text = _("File copy operation")
				progreso.secondary_text = _("Copying to PC")
				progreso.show()
				
				progreso.progress_fraction = 0
				fraction_counter = 0

				self.idle_events()
				for selected in current_selection:
					iterator, file_object = self.get_file_object(selected)
					
					progreso.secondary_text = _("Copying file: %s") % (file_object.Name)
					fraction_counter += my_fraction
					progreso.progress_fraction = fraction_counter
					self.idle_events()
					
					file_list.append(self.engine.GetFileData(file_object.Id))
				
				archivos = "\n".join(file_list)
				selection.set (selection.target, 8, archivos)
				
				progreso.progress_fraction = 1
				progreso.destroy()
			else:
				pass
			
			if context.action == gtk.gdk.ACTION_MOVE:
				context.finish(True, True, eventTime)
		return

	def __del__(self):
		if self.engine.isConnected():
			self.engine.Disconnect()

	def icon_key_press(self, object, event):
		if gtk.gdk.keyval_name(event.keyval) == 'Delete':
			selection = object.get_selected_items()
			total_items = len(selection)
			if total_items>0:
				response = message_box( _("Confirmation"),  _("Are you sure you want to delete?"), (_('Ok'),_('Cancel')), gtk.STOCK_DIALOG_QUESTION)
				if response == _('Ok'):
					for fItem in selection:
						iterator, file_object = self.get_file_object(fItem)
					
						if self.engine.DeleteFile(file_object.Id):
							self.filestore.remove(iterator)
					
					self.update_memory_status()
					self.ClearInfo()

	def drag_data_received(self, widget, context, x, y, selection, targetType, time):
		"Method that receives files from external sources "

		progreso = HigProgress()
		
		progreso.primary_text = _("File(s) Received")
		progreso.secondary_text = _("Copying to phone")
		progreso.show()
		
		self.idle_events()

		direcciones = selection.get_uris()
		
		if len(direcciones) > 0 and targetType == self.TARGET_URL:
			for direccion in direcciones:
				ruta = urlparse(direccion)[2]

				logging.debug("Receiving file: %s" % ruta)
				
				if ruta.find(".gmobilemedia/cache") > -1:
					# A file from the memory cache, trying to be added to itself
					# could add a prefix or suffix to let it copy to memory, for now just skip it
					continue
					
				try:
					myFile = self.engine.AddFile(ruta, self.CurrentFolder)
				except gammu.ERR_MEMORY:
					message_box(_("Error"), _("You can not add that file to the phone memory"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
					myFile = None

				progreso.secondary_text = ruta
				self.idle_events()
				
				if myFile:
					file_type, icon_image = self.get_mime_data(myFile.Name)
					self.filestore.append([myFile.Name, icon_image, myFile.Id])
				else:
					message_box(_("Error"), _("An error happened while saving the file"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
		
		self.update_memory_status()
		
		progreso.destroy()
		context.finish(True, False, time)	

	def preferences(self, *args):
		vPreferences = wndPreferences(self.settings)
		vPreferences.show()

	def refresh(self, *args):
		if self.engine.isConnected():
			progreso = HigProgress()
			progreso.show()
			
			progreso.primary_text = _("Reading phone data")
			progreso.secondary_text = _("Scanning phone")
			self.read_structure(progreso)
			
			progreso.destroy()
		else:
			message_box(_("Error"), _("You must connect the phone first"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
	
	def update_memory_status(self):
		if self.engine.isConnected():
			try:
				current_memory = self.engine.GetPhoneMemory()
				self.set_status_text( _("Memory free: %s / Memory used: %s") % (getReadableSize(current_memory['Free']), getReadableSize(current_memory['Used'])) )
			except TypeError:
				self.set_status_text(_("Free memory not available"))
	
	def read_structure(self, progreso):
		if self.engine.isConnected():
			progreso.secondary_text = _("Getting phone details")
			progreso.progress_fraction= 0.2
			self.idle_events()
			
			phone_data = self.engine.GetPhoneDetails()
			phone_text = (_("<b>Manufacturer:</b> %s\n<b>Model:</b> %s")) % (phone_data['manufacturer'], phone_data['model'])

			self.update_memory_status()

			self.lblStatus.set_label(phone_text)
		
			progreso.primary_text = _("Getting phone folders")
			progreso.secondary_text = _("Scanning Phone")
			progreso.progress_fraction = 0.5
			self.idle_events()
			
			try:
				if self.engine.GetFolders(progreso, self):
					progreso.secondary_text = _("Creating structure")
					progreso.progress_fraction = 0.7
					self.idle_events()
					
					self.DrawTree()
			except gammu.ERR_NOTSUPPORTED:
				message_box(_("Error"), _("Your phone model or your phone configuration is not supported at this time"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
				progreso.destroy()
		else:
			message_box(_("Refresh"), _("You must connect the phone first"), (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)


	def Conectar(self, boton):
		if not self.engine.isConnected():
			if (self.settings.is_valid()):
				progreso = HigProgress()
				
				progreso.primary_text = _("Connecting with phone")
				progreso.secondary_text = _("Initializing connection")
				progreso.show()
				
				self.idle_events()
				try:
					self.engine.Connect()
				except gammu.ERR_DEVICEOPENERROR, e:
					message_box(_("Error"), _("Seems like you haven't configured propperly your phone permissions, check if your user has rights to write to the device or if it belongs to the dialout group, message was: ") + e[0]['Text'], (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
					progreso.destroy()
					return None
				except gammu.ERR_UNKNOWNMODELSTRING, e:
					message_box(_("Error"), e[0]['Text'], (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
					progreso.destroy()
					return None
				except gammu.ERR_TIMEOUT, e:
					message_box(_("Error"), e[0]['Text'], (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
					progreso.destroy()
					return None
				except gammu.ERR_UNKNOWNCONNECTIONTYPESTRING, e:
					message_box(_("Error"), e[0]['Text'], (_('Ok'),) , gtk.STOCK_DIALOG_ERROR)
					progreso.destroy()
					return None

				self.idle_events()
				self.read_structure(progreso)

				progreso.destroy()
				if self.engine.isConnected():
					boton.set_label(_("Disconnect"))
			else:
				message_box(_("Error"), _("You must configure your connection first or use the configuration wizard"), (_('Ok'),),  gtk.STOCK_DIALOG_ERROR)
				
		else:
			boton.set_label(_("Connect"))
			self.filestore.clear()
			self.treestore.clear()
			self.engine.Disconnect()
			#~ self.fTree.disconnect('drag_data_received')

	def DrawTree(self):
		def findParent(model, search, column = 2):
			if not model : return None

			for hoja in model:
				if hoja[column] == search:
					return hoja.iter

				result = findParent(hoja.iterchildren(), search)
				if result: return result
			return None

		self.treestore.clear()
		self.filestore.clear()

		for item in self.engine.Folders:
			#if item.Level > 1: #and item.Level < 4:
			raiz = findParent(self.treestore, item.Parent)
			row = [item.Name, item.Parent, item.Id]
			leaf = self.treestore.append(raiz, row)

        def idle_events(self):
                while gtk.events_pending():
                        gtk.main_iteration()

	def help_about(self, menuItem):
		def btnClose(widget, *args):
			widget.destroy()
			
		ventana = "wndAcerca"
		myTree = gtk.glade.XML(configuration.GLADE_FILE, ventana, configuration.TEXT_DOMAIN)
		vAcercaDe = myTree.get_widget(ventana)
		vAcercaDe.connect("response", btnClose)
		vAcercaDe.set_modal(True)
		vAcercaDe.show()


	def FileInfo(self, listview):
		selection = listview.get_selected_items()
		total_items = len(selection)

		if total_items>0:
			mLabel = self.wTree.get_widget("lblDetails")
			
			if total_items==1:
				progreso = HigProgress()

				progreso.primary_text = _("File data")
				progreso.secondary_text = _("Getting file info")
				progreso.progress_fraction = 0.25
				progreso.show()

				self.idle_events()
				
				iterator, file_object = self.get_file_object(selection[0])
				
				sDetails = ("<b>Information Panel</b>\n\n<b>Name:</b><tt>%s</tt>\n<b>Size:</b> <tt>%s</tt>\n") % (file_object.Name, getReadableSize(file_object.Size))
				mLabel.set_label(sDetails)
				
				progreso.secondary_text = _("Finding mime data")
				progreso.progress_fraction = 0.50
				self.idle_events()

				file_type, icon_image = self.get_mime_data(file_object.Name)
				
				if file_type == "image":
					progreso.secondary_text = _("Loading file")
					progreso.progress_fraction = 0.75

					# Sets a temporary image while loading the real file
					self.ImageControl.set_from_pixbuf(icon_image)
					self.idle_events()
					
					try:
						# Loads the real file
						self.ImageControl.set_from_file(self.engine.GetFileData(file_object.Id))
					except gammu.ERR_TIMEOUT:
						message_box(_("Error"), _("Operation timed out, seems like you lost your connection with the phone"), (_('Ok'),),  gtk.STOCK_DIALOG_ERROR)
						progreso.destroy()
						return None
				else:
					self.ImageControl.set_from_pixbuf(icon_image)
					
				progreso.destroy()
			else:
				total_size = 0
				
				for single_file in selection:
					iterator, file_object = self.get_file_object(single_file)
					total_size += file_object.Size
				sDetails = (_("<b>Information Panel</b>\n\n<b>Group:</b><tt>%s files</tt>\n<b>Size:</b> <tt>%s</tt>\n")) % (total_items, getReadableSize(total_size))
				mLabel.set_label(sDetails)
				self.ImageControl.set_from_stock("gtk-print-preview", 6)

	def ClearInfo(self):
		 mLabel = self.wTree.get_widget("lblDetails")
		 mLabel.set_label(_("<b>Information Panel</b>"))
		 self.ImageControl.set_from_stock("gtk-print-preview", 6)

	def FolderSelect(self, treeview):
		self.ClearInfo()
		(model, iter) = treeview.get_selected()
		try:
			folderId = model.get_value(iter, 2)
			self.CurrentFolder = folderId
			self.ShowFiles(folderId)
		except:
			pass
		#~ target = [('text/uri-list', 0, 0)]
		#~ self.fTree.drag_dest_set(gtk.DEST_DEFAULT_ALL, target, gtk.gdk.ACTION_COPY)
		#~ self.fTree.connect('drag_data_received', self.drag_data_received)


	def ShowFiles(self, FolderId):
		self.filestore.clear()
		fileList = self.engine.GetFiles(FolderId)
		for myFile in fileList:
			filet_type, icon_image = self.get_mime_data(myFile.Name)
			self.filestore.append([myFile.Name, icon_image, myFile.Id])
			#~ self.filestore.append([myFile.Name, None, myFile.Id])
	
	def get_mime_data(self, filename):
		"""Tries to identify the default mime data for each file"""
		
		icon_image = None
		
		if filename.count(".")>0:
			extension = filename.split(".")[-1].lower()
		else:
			extension = ""
	
		if configuration.MIME_EXTENSIONS.has_key(extension):
			icon_image = configuration.MIME_EXTENSIONS[extension]['pixbuf']
			fileType = configuration.MIME_EXTENSIONS[extension]['type']
		else:
			icon_image = configuration.MIME_EXTENSIONS['unknown']['pixbuf']
			fileType = "unknown"

		#~ image_filename = configuration.CURRENT_PATH + os.sep + "ui" + os.sep + "gnome-mime.svg"
		
		return fileType, icon_image
		
	def set_status_text(self, text):
		context_id = self.statusbar.get_context_id("gmobilemedia")
		
		self.statusbar.pop(context_id)
		self.statusbar.push(context_id, text)

	def get_file_object(self, position):
		"""Finds the corresponding file object associated with the current list store
		Returns the iterator of the item and the file object associated
		"""
		
		try:
			tIter = self.filestore.get_iter(position)
			fileID = self.filestore.get_value(tIter,2)
			fObj = self.engine.GetFileObject(fileID)
		except:
			tIter = None
			fObj = None
			
		return tIter, fObj


def main(argv):
	app = mbGui()
	gtk.main()

if __name__ == '__main__':
        main(sys.argv)
