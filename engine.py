#!/usr/bin/env python
# -*- coding: utf-8 -*-

###
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
###

import gammu
import user, os
from datetime import datetime
import logging
from helper import *
import configuration
import locale, gettext

locale.setlocale(locale.LC_ALL, '')
gettext.install(configuration.TEXT_DOMAIN, configuration.LOCALE_DIR, unicode=1)


class MobileBrowser:
	"Mobile manager"
	sMachine = None
	Folders = []
	folder_names = {}
	Files= []
	_Data = {}
	InvalidFolders = ["Openwavetrunk", "Brew", "Messages", "PBookImg", "Srvms", "WDPRoot_Folder", "Openwavetrunk" , "Hidden", 'predeftemp', 'predefomadm', 'predefsyncml', 'serviceapplication']
	#~ InvalidFolders = []
	Connected = False

	def __init__(self):
		logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
		#~ logging.getLogger().setLevel(logging.DEBUG)
		
	def __del__(self):
		pass

	def Connect(self):
		"Init gammu module and connect to phone"
	
		if self.isConnected():
			return True
	
		try:
			self.sMachine = gammu.StateMachine()
			self.sMachine.ReadConfig()
			self.sMachine.Init()
			
			self.Connected = True
			logging.info("Connected")
			
		except gammu.ERR_DEVICENOTEXIST:
			self.Connected = False
			logging.error("Device not available")
			return False
		except IOError:
			self.Connected = False
			logging.error("Configuration non existant")
	
		return True

	def isConnected(self):
		return self.Connected

	def Disconnect(self):
		self.Connected = False
		self.sMachine.Terminate()

	def GetPhoneMemory(self):
		"Returns the free and used memory if supported by phone"
		try:
			PhoneMemory = self.sMachine.GetFileSystemStatus()
		except :
			PhoneMemory = None
			
		return PhoneMemory

	def GetPhoneDetails(self):
		PhoneData = {}

		PhoneData['manufacturer'] = self.sMachine.GetManufacturer()
		PhoneData['model'] = " ".join(self.sMachine.GetModel())
		
		if PhoneData['manufacturer'].upper() == 'NOKIA':
			self.folder_names = {
				'predefgallery': _("Gallery"),
				'predefhiddenfolder' : _('Hidden Folder'),
				'predefrecordings':  _('Recordings'),
				'predefphotos':  _('Photos'),
				'predefthemes':  _('Themes'),
				'predefthemepreview':  _('Theme preview'),
				'predefthemeactive':  _('Theme active'),
				'predefgraphics':  _('Graphics'),
				'predeftones':  _('Tones'),
				'predefcliparts':  _('Cliparts'),
				'predefreceived':  _('Received files'),
				'predefvideos':  _('Videos'),
				'predefpresencelogo':  _('Presence Logo'),
				'predefinfofolder':  _('Info Folder'),
				'predeffiledownload':  _('File downloads'),
				'predeffilereceived':  _('Received files'),
				'predeffilerecieved':  _('Received files'),
				'predefmessages' : _("Messages"),
				'predefdrafts' : _("Drafts"),
				'predefsent' : _("Sent"),
				'predefoutbox' : _("Outbox"),
				'predefinbox' : _("Inbox"),
				#~ predefinfofolder
				#~ predefomadm
				#~ serviceapplication
				#~ predeffilerecieved
				#~ predeffiledownload
				#~ predefsyncml
				}
		
		self._Data = PhoneData
		
		return PhoneData


	def AddFile(self, url, FolderId = 0):
		if os.path.exists(url):
			logging.debug("Adding file %s" % url)
			folder = self.GetFolderObject(FolderId)
			
			file_handle = open(url, "r")
			file_stat = os.stat(url)
			ttime = datetime.fromtimestamp(file_stat[8])

			Filename = os.path.basename(url)
			Fullname =  str(FolderId) ##+ os.sep + Filename

			logging.debug("Basename : %s" % Filename)
			logging.debug("Fullpath : %s" % Fullname)
			logging.debug("Folder Level : %d" % (folder.Level + 1))
			
			
			file_f = {
			"ID_FullName": Fullname,
			"Name": Filename,
			"Modified": ttime,
			"Folder": 0,
			"Level": (folder.Level + 1),
			"Used": file_stat[6],
			"Buffer": file_handle.read(),
			"Type": "Other",
			"Protected": 0,
			"ReadOnly": 0,
			"Hidden": 0,
			"System": 0,
			"Handle": 0,
			"Pos": 0,
			"Finished": 0
			}
			try :
				while (not file_f["Finished"]):
					file_f = self.sMachine.AddFilePart(file_f)
				
				tmpFile = FileObject(file_f)
				tmpFile.Parent = FolderId
				
				self.Files.append(tmpFile)
				
				return tmpFile
			except IOError:
				logging.debug("died during copy operation")
				return False
			except gammu.ERR_FILENOTEXIST, error:
				logging.debug("died during copy operation: %s" % error[0] ) 
				return False

	def NextFile (self, start = 0):
		# GetNextFileFolder gives us a dict with:
		# 'Name' => Symbolic Name of the file (String)
		# 'ID_FullName' => unique ID of the file (String)
		# 'Used' => space used in bytes (integer)
		# 'Modified' => Date of last change (datetime)
		# 'Type' => Filetype as reported by phone (String)
		# 'Folder' => Entry is a Folder (Bool)
		# 'Level' => On which level of FS (Integer)
		# 'Buffer' => ?? (String)
		# File Attributes (Bool):
		# 'Protected'
		# 'ReadOnly'
		# 'Hidden'
		# 'System'
		file = None;
		try:
			file = self.sMachine.GetNextFileFolder(start);
		except gammu.ERR_EMPTY:
			pass
		except ValueError:
			#Unknown error when reading folders from Nokia 5200
			pass

		return file
	
	def GetFolders(self, progress, window):
		"Traverses all the file structure to make future operations faster"
		
		logging.info("Getting Folders structure")
		
		try:
		    file = self.NextFile(1)
		except gammu.ERR_NOTCONNECTED:
		    logging.error("Not connected")
		    return False
		
		#Removes all items from the list
		self.Folders = []
		self.Files = []
		
		#Temporary dictionary to store the parent ID
		Parents = {}

		while(file):
			oFile = FileObject(file)

			if oFile.Level>1:
				#Gets the Id from the last folder in the previous level
				oFile.Parent = Parents[(oFile.Level - 1)][-1]
			else:
				oFile.Parent = 0
	
			#~ print ("%s - %s"  ) % (oFile.Parent, oFile.Name)
			if oFile.isFolder(): #and oFile.isUsable():
				if not Parents.has_key(oFile.Level):
					Parents[oFile.Level] = []
			
				if self.folder_names.has_key(oFile.Name):
					oFile.Name = self.folder_names[oFile.Name]
			
				Parents[oFile.Level].append(oFile.Id)

				logging.debug("Folder Name %s" % oFile.Name)
				self.Folders.append(oFile)
				
				progress.secondary_text = oFile.Name
				window.idle_events()
			else:
				#~ logging.debug("Folder: %s, File: %s" % (oFile.Parent, oFile.Name))
				self.Files.append(oFile)

			file = self.NextFile()

		self.CleanFolders()
		logging.info("Folder structure read")
		return True

	def GetByName(self, name):
		"Returns an specific folder item based on it's name"

		for item in self.Folders:
			if item.Name.startswith(name):
				return item

		return None

	def CleanFolders(self):
		"""Removes the folders that should not appear in the main view, it is based on an existing list"""
		for iFolder in self.InvalidFolders:
			FolderItem = self.GetByName(iFolder)
			if FolderItem:
				self.deleteBranch(FolderItem)

	def deleteBranch(self, FItem):
		"""Deletes an specific branch from the tree structure, is only applied to memory,
		doesn't write anything to the phone"""
		subfolders = [subf for subf in self.Folders if subf.Parent == FItem.Id ]

		if subfolders:
			for folder in subfolders:
				self.deleteBranch(folder)

		self.Folders.remove(FItem)
		return

	def DeleteFile(self, fileId):
		"Deletes a file from the phone filesystem"
		try:
			self.sMachine.DeleteFile(fileId)
			self.Files.remove(self.GetFileObject(fileId))
			
			return True
		except gammu.ERR_FILENOTEXIST:
			return False

	def GetFiles(self, folderId):
		file_list = []
		#~ file_debug = []
		for item in self.Files:
			if item.Parent == folderId:
				file_list.append(item)
				#~ file_debug.append(item.Name)
			#~ logging.debug("Folder: %s, File: " % (item.Parent, item.Name))
		
		#~ print file_debug
		return file_list

	def GetFileObject(self, FileId):
		for file in self.Files:
			if file.Id == FileId:
				return file

	def GetFolderObject(self, FileId):
		for file in self.Folders:
			if file.Id == FileId:
				return file

	def GetFileData(self, FileId):
		fObj = self.GetFileObject(FileId)
		
		path = user.home + os.sep + ".gmobilemedia" + os.sep+ "cache"
		
		if not os.path.isdir(path):
		    os.makedirs(path, mode=0700)
		
		#Get a better way to differentiate each object #fObj.Id + "_"
		FilePath = path + os.sep +fObj.Name
		
		# Checks if it has already been downloaded before
		# and compares its filesize
		if os.path.exists(FilePath):
			filesize = os.path.getsize(FilePath)
			if filesize == fObj.Size:
				return FilePath


		f = file(FilePath, 'w')
		file_f = {
		    "ID_FullName": fObj.Id,
		    "Finished": 0
		}
		while (not file_f["Finished"]):
			file_f = self.sMachine.GetFilePart(file_f)
		f.write(file_f["Buffer"])
		f.flush();

		return FilePath

class FileObject:
	"Structure used to hold files and folders information"
	
	Id = u''
	Size = 0
	Name = u''
	Level = 0
	Folder = False
	Hidden =  0
	Parent = u'0'
	ReadOnly  = False

	def __init__(self, object = None):
		self.Bind(object)
		
	def __str__(self):
		return "File: " % self.Name

	def Bind(self, Object):
		"Attaches a gammu file object to a simpler structure"
		if Object != None:
			self.Name = Object['Name']
			self.Id = Object['ID_FullName']
			self.Level = Object['Level']
			self.Folder = Object['Folder']
			self.Hidden = Object['Hidden']
			self.ReadOnly = Object['ReadOnly']
			self.Size = float(Object['Used'])

			return True
		else:
			return None

	def isFolder(self):
		return self.Folder

	def isUsable(self):
		if not self.Hidden and not self.ReadOnly:
			return True
		else:
			return False
