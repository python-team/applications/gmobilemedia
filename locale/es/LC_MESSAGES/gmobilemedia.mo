��    5      �  G   l      �     �  R   �  (   �  	        (  %   ;  '   a  >   �      �     �     �                    -     >     O     `     n       
   �  
   �     �     �     �     �  	   �     �               1     C     U     k     �     �  !   �     �     �     �     �     �            �  '     &  H   .      w     �     �  )   �     �  y  �     [  \   x  '   �     �       &   "  4   I  _   ~  "   �                     .     7     O     d     w     �     �     �     �     �     �     �          !     A     R     e     y     �  "   �  !   �  !   �          *  )   :     d     l     �  	   �     �     �     �  g  �  	   f  N   p  #   �     �     �  .   �     "        .   ,                  	             2      3                #      '         )      "       4   (   *       5                                   &       1   !          +                         /   0   -                   
          $           %    <b>Information Panel</b> <b>Information Panel</b>

<b>Group:</b><tt>%s files</tt>
<b>Size:</b> <tt>%s</tt>
 <b>Manufacturer:</b> %s
<b>Model:</b> %s A_bout... About gMobileMedia Allows connecting to te current phone An error happened while saving the file Application to handle Mobile phones filesystem on Nokia phones Are you sure you want to delete? Basic preferences Cancel Confirmation Connect Connecting with phone Connection Port: Connection type: Copying file: %s Copying to PC Copying to phone Creating structure Date/Time: Disconnect Drag an item to copy it Error Errors logfile: File copy operation File data File(s) Received Finding mime data Free memory not available Gammu Preferences Getting file info Getting phone details Getting phone folders Initializing connection Loading file Memory free: %s / Memory used: %s Ok Phone Model: Reading phone data Refresh Scanning Phone Scanning phone Shows the phone items This package is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2 dated
June, 1991.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free 
Software Foundation, Inc., 59 Temple Place - Suite 330, 
Boston, MA 02111-1307, USA.

On Debian GNU/Linux systems, the complete text of the
GNU General Public License can be found in
`/usr/share/common-licenses/GPL'. Website You must configure your connection first or use the configuration wizard You must connect the phone first _File _Help _Synchronize date and time with computer? translator-credits Project-Id-Version: 0.1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-12-02 02:21-0500
PO-Revision-Date: 2006-12-02 04:01-0500
Last-Translator: Iván Campaña <ivan.campana@gmail.com>
Language-Team: es <ivan.campana@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Spanish
X-Poedit-SourceCharset: utf-8
 <b>Panel de información</b> <b>Panel de información</b>

<b>Grupo:</b><tt>%s archivos</tt>
<b>Tamaño:</b> <tt>%s</tt>
 <b>Fabricante:</b> %s
<b>Modelo:</b> %s A_cerca de... Acerca de gMobileMedia Permite conectarse al teléfono actual Un error ha ocurrido mientras se guardaba el archivo Aplicación para manejar el sistema de archivos de los teléfonos móviles en teléfonos Nokia. ¿Está seguro que desea eliminar? Preferencias básicas Cancelar Confirmación Conectar Conectando al teléfono Puerto de conexión: Tipo de conexión: Copiando archivo: %s Copiando al PC Copiando al teléfono Creando estructura Hora/Fecha: Desconectar Arrastre un item para copiarlo Error Archivo de registro de errores: Operación de copia de archivos Datos de archivo Archivos Recibidos Buscando datos mime Memoria Libre no disponible Preferencias de gammu Obteniendo información de archivo Obteniendo detalles del teléfono Obteniendo carpetas del teléfono Inicializando conexión Leyendo archivo Memoria libre: %s / Memoria utilizada: %s Aceptar Modelo de teléfono Leyendo datos del teléfono Refrescar Inspeccionando el teléfono Inspeccionando el teléfono Muestra los items del teléfono Este paquete es software libre; puede redistribuirlo y/o
modificarlo bajo los términos de la Licencia Pública General GNU
como fuere publicado por la Free Software Foundation; version 2 con fecha
 Junio, 1991.

Este paquete es distribuido con la esperanza de que sea útil,
pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita
de MERCANTIBILIDAD Sitio web Debe configurar primero su conexión o utilizar el asistente de configuración Debes conectar el teléfono primero _Archivo A_yuda ¿_Sincronizar fecha y hora con el computador? Iván Campaña N. 