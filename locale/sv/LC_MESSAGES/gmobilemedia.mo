��    5      �  G   l      �     �  R   �  (   �  	        (  &   ;  '   b  >   �      �     �     �                    .     ?     P     a     o     �  
   �  
   �     �     �     �     �  	   �     �               2     D     V     l     �     �  !   �     �     �     �     �     �            �  (     '  H   /      x     �     �  )   �     �  K  �     .  U   G  (   �     �     �  )   �  *     ;   2  &   n     �     �     �     �     �     �     �               '     =  
   M     X  #   e     �     �     �     �     �     �  #   �               2     J     `     u  $   �     �     �     �  	   �     �     �     �  Y    	   i  O   s  &   �     �     �  '   �  &   !        .   ,                  	             2      3               #      '         )      "       4   (   *       5                                   &       1   !          +                  0       /       -                   
          $           %    <b>Information Panel</b> <b>Information Panel</b>

<b>Group:</b><tt>%s files</tt>
<b>Size:</b> <tt>%s</tt>
 <b>Manufacturer:</b> %s
<b>Model:</b> %s A_bout... About gMobileMedia Allows connecting to the current phone An error happened while saving the file Application to handle Mobile phones filesystem on Nokia phones Are you sure you want to delete? Basic preferences Cancel Confirmation Connect Connecting with phone Connection Port: Connection type: Copying file: %s Copying to PC Copying to phone Creating structure Date/Time: Disconnect Drag an item to copy it Error Errors logfile: File copy operation File data File(s) Received Finding mime data Free memory not available Gammu Preferences Getting file info Getting phone details Getting phone folders Initializing connection Loading file Memory free: %s / Memory used: %s Ok Phone Model: Reading phone data Refresh Scanning Phone Scanning phone Shows the phone items This package is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2 dated
June, 1991.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free 
Software Foundation, Inc., 59 Temple Place - Suite 330, 
Boston, MA 02111-1307, USA.

On Debian GNU/Linux systems, the complete text of the
GNU General Public License can be found in
`/usr/share/common-licenses/GPL'. Website You must configure your connection first or use the configuration wizard You must connect the phone first _File _Help _Synchronize date and time with computer? translator-credits Project-Id-Version: gmobilemedia
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-12-02 02:21-0500
PO-Revision-Date: 2007-04-24 11:11-0500
Last-Translator: Iván Campaña <ivan.campana@gmail.com>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 <b>Informationspanel</b> <b>Informationspanel</b>

<b>Grupp:</b><tt>%s filer</tt>
<b>Storlek:</b> <tt>%s</tt>
 <b>Tillverkare:</b> %s
<b>Modell:</b> %s O_m... Om gMobileMedia Tillåter anslutning till aktuell telefon Ett fel inträffade vid skrivning av filen Program för att hantera filsystem på Nokia-mobiltelefoner Är du säker på att du vill ta bort? Grundläggande inställningar Avbryt Bekräftelse Anslut Ansluter till telefon Anslutningsport: Anslutningstyp: Kopierar fil: %s Kopierar till PC Kopierar till telefon Skapar struktur Datum/Tid: Koppla från Dra ett objekt för att kopiera det Fel Loggfil med fel: Filkopieringsåtgärd Fildata Fil(er) mottagna Letar mime-data Ledigt minne är inte tillgängligt Inställningar för Gammu Hämtar filinformation Hämtar telefondetaljer Hämtar telefonmappar Initierar anslutning Läser in fil Ledigt minne: %s / Använt minne: %s Ok Telefonmodell: Läser telefondata Uppdatera Söker av telefon Söker av telefon Visar telefonobjekt Detta program är fri programvara. Du kan distribuera det och/eller
""modifiera det under villkoren i GNU General Public License, publicerad
""av Free Software Foundation, antingen version 2 eller (om du så vill)
""någon senare version.
""
""Detta program distribueras i hopp om att det ska vara användbart,
""men UTAN NÅGON SOM HELST GARANTI, även utan underförstådd garanti
""om SÄLJBARHET eller LÄMPLIGHET FÖR NÅGOT SPECIELLT ÄNDAMÅL. Se
""GNU General Public License för ytterligare information.
""
""Du bör ha fått en kopia av GNU General Public License tillsammans
""med detta program. Om inte, skriv till Free Software Foundation,
""Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
"
"På Debian GNU/Linux-system kan den fullständiga texten för
GNU General Public License hittas i filen
"/usr/share/common-licenses/GPL". Webbplats Du måste konfigurera din anslutning först eller använda konfigurationsguiden Du måste anslut till telefonen först _Arkiv _Hjälp _Synkronisera datum och tid med datorn? Daniel Nylander <po@danielnylander.se> 